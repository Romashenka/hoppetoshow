﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class someBounce : MonoBehaviour {

	Rigidbody myRb;

	// Use this for initialization
	void Start () {
		myRb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetAxis ("Vertical") != 0) {
			myRb.AddForce (Vector3.up * Input.GetAxis ("Vertical")*10);
		}
	}

	void OnCollisionEnter(Collision collision){
		myRb.AddForce (collision.impulse, ForceMode.Impulse);
	}
}

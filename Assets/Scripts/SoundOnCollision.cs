﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundOnCollision : MonoBehaviour {

	AudioSource audioSource;

	void Start(){
		audioSource = GetComponent <AudioSource>();
	}

	void OnCollisionEnter(){
		audioSource.Play ();
	}
}

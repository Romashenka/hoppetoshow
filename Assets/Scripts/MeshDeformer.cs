﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshDeformer : MonoBehaviour {

	public float collisionForceMultiplier = 10f;
	public float collisionForceOffset = .1f;
	public float springForce = 20f;
	public float damping = 5f;

	float uniformScale = 1f;

	Mesh deformingMesh;
	Vector3[] originalVertices, displacedVertices,vertexVelocities;


	void Start () {
		deformingMesh = GetComponent<MeshFilter>().mesh;
		originalVertices = deformingMesh.vertices;
		displacedVertices = new Vector3[originalVertices.Length];
		vertexVelocities = new Vector3[originalVertices.Length];
		for (int i = 0; i < originalVertices.Length; i++) {
			displacedVertices[i] = originalVertices[i];
		}
	}

	public void AddDeformingForce (Vector3 point, float force) {
		point = transform.InverseTransformPoint(point);
		Debug.DrawLine(Camera.main.transform.position, point);
		for (int i = 0; i < displacedVertices.Length; i++) {
			AddForceToVertex(i, point, force);
		}
	}

	void AddForceToVertex (int i, Vector3 point, float force) {
		Vector3 pointToVertex = displacedVertices[i] - point;
		pointToVertex *= uniformScale;
		float attenuatedForce = force / (1f + pointToVertex.sqrMagnitude);
		float velocity = attenuatedForce * Time.deltaTime;
		vertexVelocities[i] += pointToVertex.normalized * velocity;
	}

	void Update () {
		uniformScale = transform.localScale.x;
		for (int i = 0; i < displacedVertices.Length; i++) {
			UpdateVertex(i);
		}
		deformingMesh.vertices = displacedVertices;
		deformingMesh.RecalculateNormals();
		deformingMesh.RecalculateBounds ();
	}

	void OnCollisionEnter(Collision col){
		
		Vector3 forcePoint = col.contacts [0].point + col.contacts [0].normal * collisionForceOffset;
		AddDeformingForce (forcePoint, col.relativeVelocity.magnitude*collisionForceMultiplier);

	}

	void UpdateVertex (int i) {
		Vector3 velocity = vertexVelocities[i];
		Vector3 displacement = displacedVertices[i] - originalVertices[i];//displacement from original shape
		displacement *= uniformScale;//dependence from local scale
		velocity -= displacement * springForce * Time.deltaTime;//add force and displacement to velocity
		velocity *= 1f - damping * Time.deltaTime;//add some pamping
		vertexVelocities[i] = velocity;
		displacedVertices [i] += velocity * (Time.deltaTime/uniformScale);

	}
}

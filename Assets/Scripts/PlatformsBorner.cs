﻿using UnityEngine;

public class PlatformsBorner : MonoBehaviour {
	/*
	public Transform startPlatform; // первая платформа для точной установки её под шар

	public GameObject platformPrefab; 

	Transform lastPlatform; // последняя установленная на старте платформа. на её прозицию впоследствии ставятся новые платформы.

	SphereMotor sphereMotor; // для подписки на события и установки позиции первой платформы

	Vector3 rangeBetweenPlatforms; 

	Vector3 startPosition; 

	Vector3 bornPosition; // позиция для респавна платформ

	void Start(){
		sphereMotor = GetComponent<SphereMotor> ();
		startPosition = GetPlatformStartPosition ();
		startPlatform.position = startPosition;
		lastPlatform = startPlatform;
		rangeBetweenPlatforms = new Vector3(0,0,GameManager.instance.gameData.platformDisplacement);
		sphereMotor.OnPlatformChecked += SphereMotor_OnPlatformChecked;
		SetPlatforms (5);
	}

	void SphereMotor_OnPlatformChecked(){
		BornPlatform ();
	}

	Vector3 GetPlatformStartPosition(){
		float sphereRadius = sphereMotor.GetComponent<SphereCollider> ().radius*sphereMotor.transform.lossyScale.y;
		float boxHigh = platformPrefab.GetComponent<BoxCollider> ().size.y / 2f*platformPrefab.transform.lossyScale.y;
		Vector3 result = sphereMotor.transform.position;
		result.y -= sphereRadius + boxHigh;
		return result;
	}

	void SetPlatforms(int howMuch){
		for (int i = 0; i < howMuch; i++) {
			rangeBetweenPlatforms.x = RandomizePosition (startPosition.x, GameManager.instance.gameData.platformXAxisMaxOffset);
			Vector3 positionToInstantiate = lastPlatform.position;
			positionToInstantiate.x = startPosition.x;
			positionToInstantiate += rangeBetweenPlatforms;
			lastPlatform = Instantiate (platformPrefab, positionToInstantiate, platformPrefab.transform.rotation).transform;
		}
		bornPosition = lastPlatform.position;
	}

	void BornPlatform(){
		bornPosition.x = RandomizePosition(startPosition.x, GameManager.instance.gameData.platformXAxisMaxOffset);
		lastPlatform = Instantiate (platformPrefab,bornPosition, platformPrefab.transform.rotation).transform;
	}

	float RandomizePosition(float valueToRandomize,float maxOffset){
		return valueToRandomize +  Random.Range (-maxOffset, maxOffset);
	}
	*/
}

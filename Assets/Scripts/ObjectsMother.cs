﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Grid))]
public class ObjectsMother : MonoBehaviour {
	
	public List<ObjectsMotor> objectsToBorn;
	public List<float> objectsProbability;
	public float bornPeriod;
	public Transform bornPosition; 

	float time;

	Grid grid ;

	float totalProb = 0;



	void Start(){
		grid = GetComponent<Grid> ();
		SetTotalProbability (out totalProb);
	}
		
	void Update(){
		BornObjects ();
	}

	void SetTotalProbability(out float totalProb){
		totalProb = 0f;
		int r = objectsProbability.Count - objectsToBorn.Count;
		if (r < 0) {
			for (int i = r; i < 0; i++) {
				objectsProbability.Add (0);
			}
			Debug.Log ("objectsProbability.Count < objectsToBorn.Count. Check the inspector",this);
		} else if (r > 0) {
			Debug.Log ("objectsProbability.Count > objectsToBorn.Count. Check the inspector",this);
		}

		foreach (float f in objectsProbability) {
			totalProb += f;
		}
	}

	int GetRandomObjectIndex(List<float>probabilities){
		float rd = Random.Range (0, totalProb);
		for (int i = 0; i < probabilities.Count; i++) {
			rd -= probabilities [i];
			if (rd <= 0)
				return i;
		}
		return 0;
	}

	void BornObjects(){
		if (Time.time > time + bornPeriod) {
			time = Time.time;
			int rdCellOnGrd = Random.Range (0, grid.CellNumberX+1);
			float rdPosOnGrd = grid.GridStartPos.x + rdCellOnGrd * grid.CellSize;
			Vector3 newPos = new Vector3 (rdPosOnGrd, grid.GridStartPos.y, grid.GridEndPos.z);
			Instantiate (objectsToBorn [GetRandomObjectIndex(objectsProbability)], newPos, transform.rotation);
		}
	}


}

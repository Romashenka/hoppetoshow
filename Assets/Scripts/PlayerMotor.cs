﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class PlayerMotor : MonoBehaviour
{

    public Transform modelTransform;
    public float velocityToXAngle;
    public float maxDeviationAngleZ;
    public float maxVelocity;
    public float sinkSpeed;
    public float horizontalSpeed;
    public Vector3 initialVelocity;

    Rigidbody myRb;
    Vector3 initialGravity;
    private System.Action currentState;
    private Vector3 startEuler;


    void Start()
    {
        myRb = GetComponent<Rigidbody>();
        startEuler = transform.rotation.eulerAngles;
        GameManager.instance.OnGameStarted += Launch;
    }

    void FixedUpdate()
    {

        if (Input.GetKey(KeyCode.Space))
        {
            Sink();
        }

        float inputH = Input.GetAxis("Horizontal");
        transform.position = transform.position + Vector3.right * inputH * horizontalSpeed * Time.fixedDeltaTime;

        myRb.velocity = Vector3.ClampMagnitude(myRb.velocity, maxVelocity);

        float rotationX = -myRb.velocity.y * velocityToXAngle;
        float rotationZ = -inputH * maxDeviationAngleZ;
        transform.rotation = Quaternion.Euler(startEuler + new Vector3(rotationX, 0f, rotationZ));

    }

    void Break() { }

    void OnHoldButtonDown()
    {
        currentState = Sink;
    }

    void OnHoldButtonUp()
    {
        currentState = Break;
    }

    void Launch()
    {
        myRb.AddForce(initialVelocity, ForceMode.VelocityChange);
    }

    void Sink()
    {
        myRb.AddForce(Vector3.up * -sinkSpeed * Time.fixedDeltaTime, ForceMode.VelocityChange);

    }

    void HorizontalMove(Vector3 direction)
    {
        myRb.AddForce(new Vector3(direction.x * horizontalSpeed, myRb.velocity.y, myRb.velocity.z), ForceMode.VelocityChange);
    }

    void OnCollisionEnter(Collision collision)
    {
        myRb.AddForce(Vector3.up * collision.impulse.magnitude / myRb.mass, ForceMode.VelocityChange);
    }

}

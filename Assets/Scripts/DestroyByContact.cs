﻿using UnityEngine;

public class DestroyByContact : MonoBehaviour {
	
	void OnTriggerEnter(Collider col){
		Destroy (col.gameObject);
	}
}

﻿using UnityEngine;
using System;

[RequireComponent(typeof(GameData))]
public class GameManager : MonoBehaviour {

	public PlayerStatus playerStatus;

	public GameData gameData{ get; private set;}

	public event Action OnGameStarted;

	public event Action OnGameEnded;

	public static GameManager instance { get; private set;}

	void Awake () {
		
		if (instance != null) {
			Debug.Log ("More than one GameManager object in the scene");
			Destroy (this);
			return;
		}
		if (playerStatus == null) {
			Debug.Log ("need gameobject with PlayerStatus");
		}

		instance = this;
		gameData = GetComponent<GameData> ();
		Time.timeScale = 0;
		playerStatus.OnGameOver += EndGame;
	}

	void Update () {
		if ((!gameData.gameStarted)&&(Input.touchCount > 0||Input.GetKeyDown(KeyCode.Space))) {
			StartGame ();
			Time.timeScale = 1;
		}
	}

	void StartGame(){
			if (OnGameStarted != null)
				OnGameStarted ();
			gameData.gameStarted = true;
	}

	public void EndGame(){
		Time.timeScale = 0;
		if (OnGameEnded != null) {
			gameData.gameStarted = false;
			OnGameEnded ();
		}
	}
}

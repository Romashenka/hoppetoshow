﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlatformMotor : MonoBehaviour {
	/*
	public GameObject bonusPrefab;

	public Transform bonusPosition;

	Rigidbody myRb;

	[Range(0f,1f)]
	public float bonusChance;

	void Start(){
		if (Random.Range (1, 100) < bonusChance*100) {
			Instantiate (bonusPrefab, bonusPosition.position, bonusPosition.rotation).transform.SetParent (bonusPosition);
		}
		myRb = GetComponent<Rigidbody> ();
		GameManager.instance.OnGameStarted += Move;
		if (GameManager.instance.gameData.gameStarted == true) {
			Move ();
		}
	}

	void Update(){
		if (transform.position.z < Camera.main.transform.position.z) {
			GameManager.instance.OnGameStarted -= Move;
			Destroy (gameObject);
		}
	}


	void Move(){
		myRb.velocity = -GameManager.instance.gameData.platformVelocity;
	}


	void OnCollisionEnter(Collision collision){
		StartCoroutine (PushDown (collision));
	}

	IEnumerator PushDown(Collision collision){
		myRb.AddForce (collision.impulse, ForceMode.VelocityChange);
		yield return new WaitForSeconds (GameManager.instance.gameData.platformPushDownTime);
		myRb.constraints = RigidbodyConstraints.FreezePositionY;
	}
	*/
}

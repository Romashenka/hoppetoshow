﻿using System;
using UnityEngine;

public class LendedEventArgs{
	public int scores{ get; set;}
	public int coins{ get; set;}

}

public class ScoreCounter : MonoBehaviour {

	public static Action<LendedEventArgs> OnLended;

	public int scoreCost;
	public int coinsCost;

	void OnCollisionEnter(Collision collision){
		CountScores ();
	}

	protected void CountScores(){
		if (OnLended != null) {
			LendedEventArgs e = new LendedEventArgs ();
			e.scores = scoreCost;
			e.coins = coinsCost;
			OnLended (e);
		}
	}
}

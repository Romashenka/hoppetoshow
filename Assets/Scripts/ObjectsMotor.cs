﻿using System.Collections;
using UnityEngine;


public class ObjectsMotor : MonoBehaviour {

	public Vector3 bounceForce;
	[Header ("Breaking")]
	public float breakForce = 1000;
	public float forceRadius = 2;
	public float partsLifetime = 2;


	void FixedUpdate(){
		transform.Translate(-Vector3.forward * GameManager.instance.gameData.objectsSpeed*Time.fixedDeltaTime);
	}

	void OnCollisionEnter(Collision collision){
		collision.rigidbody.AddForce (bounceForce, ForceMode.VelocityChange);
		BreakToParts ();
	}

	void BreakToParts(){
		Rigidbody[] children = GetComponentsInChildren<Rigidbody> ();
		foreach (Rigidbody rb in children) {
			rb.useGravity = true;
			rb.AddExplosionForce (breakForce, transform.position, forceRadius);
			Destroy (rb.gameObject, partsLifetime);
		}
	}

}

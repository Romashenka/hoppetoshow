﻿using UnityEngine;

public class PauseOnEnable : MonoBehaviour
{
    private float timeScaleBeforePause;

    void OnEnable()
    {
        timeScaleBeforePause = Time.timeScale;
        Time.timeScale = 0f;
    }

    void OnDisable()
    {
        Time.timeScale = timeScaleBeforePause;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOverTime : MonoBehaviour {

	public float lifetTime;

	void Start () {
		Destroy (gameObject, lifetTime);
	}

}

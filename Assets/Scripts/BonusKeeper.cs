﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusKeeper : MonoBehaviour {

	public Transform bonusLocation; 
	public GameObject[] bonusesPrefabs;
	[Range(0,1)]
	public float bonusChance;

	void Start () {
		float rd = Random.Range (0f, 1f);
		if (bonusesPrefabs!=null && rd < bonusChance) {
			int rdIndex = Random.Range (0, bonusesPrefabs.Length);
			Instantiate (bonusesPrefabs [rdIndex], bonusLocation.position, Quaternion.identity).transform.SetParent (transform);
		}
	}
}

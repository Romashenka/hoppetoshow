﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour {

	public Text coinsCounter;
	public Text scoreCounter;
	public GameObject gameOverPanel;
    public GameObject pauseMenu;

    void Start(){
		gameOverPanel.SetActive (false);
		GameManager.instance.OnGameEnded += GameOver;
	}

	void Update(){
        coinsCounter.text = GameManager.instance.gameData.coinsCount.ToString();
        scoreCounter.text = GameManager.instance.gameData.scoreCount.ToString();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pauseMenu.SetActive(!pauseMenu.activeSelf);
        }
    }

    void GameOver(){
		gameOverPanel.SetActive (true);
	}

	public void RestartLevel(){
		SceneManager.LoadScene (1);
	}

	public void QuitToMainMenu(){
		SceneManager.LoadScene (0);

	}
}

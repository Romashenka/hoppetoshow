﻿using System;
using UnityEngine;

public class InputController : MonoBehaviour {

	public Joystick joystick;
	
	public HoldReleaseButton holdReleaseButton;
	
	public Action holdReleaseButtonUp;
	public Action holdReleaseButtonDown;

	public float HorizontalJoystick
	{
		get
		{
			return joystick.Horizontal;
		}
	}

	void Start()
	{
		holdReleaseButton.pointerUp+=holdReleaseButtonUp;
		holdReleaseButton.pointerDown+=holdReleaseButtonDown;
	}


}

﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Joystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    [Header("Options")]
    [Range(0f, 2f)] public float handleLimit = 1f;

    [Header("Components")]
    public RectTransform background;
    public RectTransform handle;

    private Vector2 lastNonzeroInput;
    private Vector2 inputVector = Vector2.zero;
    public float Horizontal { get { return InputVector.x; } }
    public float Vertical { get { return InputVector.y; } }
    public Vector2 InputVector { get { return inputVector; } protected set { inputVector = value; } }
    public Vector2 LastNonzeroInput
    {
        get
        {
            if (inputVector != Vector2.zero)
            {
                lastNonzeroInput = inputVector;
            }
            return lastNonzeroInput;
        }
    }

    public System.Action PointerUp;
    public System.Action PointerDown;
    public virtual void OnDrag(PointerEventData eventData)
    {

    }

    public virtual void OnPointerDown(PointerEventData eventData)
    {
        if (PointerDown != null)
        {
            PointerDown();
        }
    }

    public virtual void OnPointerUp(PointerEventData eventData)
    {
        if (PointerUp != null)
        {
            PointerUp();
        }
    }
}

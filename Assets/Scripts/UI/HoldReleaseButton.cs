﻿using UnityEngine;
using UnityEngine.EventSystems;


public class HoldReleaseButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    public System.Action pointerDown;
    public System.Action pointerUp;


    public void OnPointerDown(PointerEventData eventData)
    {
        if (pointerDown != null)
        {
            pointerDown();
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (pointerUp != null)
        {
            pointerUp();
        }
    }
}


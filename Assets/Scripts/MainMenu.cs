﻿using UnityEngine;

public class MainMenu : MonoBehaviour {

	public GameObject mainMenu;
	public GameObject options;

	void Start(){
		mainMenu.SetActive (true);
		options.SetActive (false);
	}

	public void StartGame(){
		UnityEngine.SceneManagement.SceneManager.LoadScene (1, UnityEngine.SceneManagement.LoadSceneMode.Single);
	}

	public void OptionsOpenClose(GameObject options){
		options.SetActive (!options.activeSelf);
	}

}

﻿using UnityEngine;

public class GameData : MonoBehaviour {

	public float objectsSpeed;

	
	//public float jumpTime = 1; //время основного прыжка

	//public float platformDisplacement = 5;  //расстояние, которое должна проходить платформа за время прыжка

	//public float platformXAxisMaxOffset; //максимальное отклонение платформ по оси Х при их рандомной постановке

	//public float platformPushDownTime; //время проседания платформы при попадании на неё шара


	//public Vector3 jumpVelocity; //вектор начальной скорости шара 

	//public Vector3 platformVelocity; //вектор постоянной скорости платформы



	public int coinsCount { get; private set;}

	public int scoreCount { get; private set;}

	public bool gameStarted { get; set; }

	static GameData instance;

	void Awake () {
		//CalculateBounceVelocityes (jumpTime, platformDisplacement, out jumpVelocity, out platformVelocity);
		if (instance == null) {
			instance = this;
			ScoreCounter.OnLended += AddStats;
		}
	}


	/*
	void CalculateBounceVelocityes(float jumpTime,float displacement,out Vector3 verticalVelocity,out Vector3 horizontalVelocity){
		horizontalVelocity = Vector3.forward * displacement / (jumpTime+2*platformPushDownTime);
		verticalVelocity = Vector3.up * -.5f * (jumpTime+2*platformPushDownTime) * Physics.gravity.y;
	}
	*/

	public void AddStats(LendedEventArgs e){
		scoreCount += e.scores;
		coinsCount += e.coins;
	}
}

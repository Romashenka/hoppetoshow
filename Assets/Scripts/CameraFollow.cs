﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
	
	public Rigidbody target;
	public float rotationSpeed;

	public float rotationScale;
	public float maxRotationXAngle;
	public float smoothness;
	Vector3 offset;
	Quaternion startRotation;

	// Use this for initialization
	void Start () {
		offset = transform.position - target.transform.position;
		startRotation = transform.rotation;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 resultPosition = target.transform.position + offset;
		transform.position = Vector3.Lerp (transform.position, resultPosition, Time.deltaTime * smoothness);

		RotateOnSink (target, rotationScale, rotationSpeed, maxRotationXAngle);
		/*
		if(Input.touchCount!=0&&Input.touches[0].phase == TouchPhase.Moved){
			Vector3 deltaTouch = Vector3.zero;
			deltaTouch.x = Input.touches [0].deltaPosition.x;
			resultPosition += deltaTouch;
		}

		*/
	}

	void RotateOnSink(Rigidbody target,float rotationScale,float rotationSpeed,float maxRotationXAngle){
		if (target.velocity.y < 0) {

			Vector3 rotation = new Vector3 (Mathf.Clamp(transform.rotation.eulerAngles.x+target.velocity.y*rotationScale,0,maxRotationXAngle),0);

			transform.rotation = Quaternion.Lerp (transform.rotation,Quaternion.Euler (rotation), rotationSpeed*Time.deltaTime);
		}
		else transform.rotation = Quaternion.Lerp(transform.rotation,startRotation,rotationSpeed*Time.deltaTime);

	}
}

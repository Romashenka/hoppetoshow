﻿using UnityEngine;

public class Breakable : MonoBehaviour {

	public float explForceValue;
	public float partsLifetime;

	void Awake(){
		Rigidbody[] children = GetComponentsInChildren<Rigidbody> ();
		transform.DetachChildren ();
		foreach (Rigidbody rb in children) {
			rb.AddExplosionForce (explForceValue, transform.position, 10f);
			Destroy (rb.gameObject,partsLifetime);
		}
		Destroy (gameObject, partsLifetime+1);
	}
}

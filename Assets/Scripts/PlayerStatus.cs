﻿using System;
using UnityEngine;

public class PlayerStatus : MonoBehaviour {

	public Action OnGameOver;
	public float lowestY;

	void Update () {
		if (transform.position.y < lowestY) {
			if (OnGameOver != null)
				OnGameOver ();
		}
	}

}

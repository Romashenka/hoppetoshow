﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour {

	[SerializeField]
	private float cellSize = 1;
	public float CellSize{ get { return cellSize; }}

	[SerializeField]
	private int cellNumberX = 10;
	public int CellNumberX{get{ return cellNumberX;}}

	[SerializeField]
	private int cellNumberZ = 10;
	public int CellNumberZ{get{ return cellNumberZ;}}

	public Vector3 GridStartPos{get{ return new Vector3 (-cellNumberX / 2 * cellSize, 0, 0) + transform.position;}}

	public Vector3 GridEndPos {
		get {
			Vector3 ep = GridStartPos;
			ep.x += cellNumberX * cellSize;
			ep.z += cellNumberZ * cellSize;
			return ep;
		}
	}


	/*
	public Vector3 GetNearestPoint(Vector3 location){
	
	
	}
	*/


	void OnDrawGizmos(){
		Gizmos.color = Color.red;
		for (int i = 0; i < cellNumberX; i++) {
			for (int j = 0; j < cellNumberZ; j++) {
				Vector3 p = new Vector3 (i * cellSize-cellNumberX/2*cellSize, 0, j * cellSize) + transform.position;
				//Vector3 p = LocationOnGrid(new Vector3(i,transform.position.y,j));
				Gizmos.DrawWireSphere (p, cellSize / 8);
			}
		
		}


	}

}

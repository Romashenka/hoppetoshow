﻿using UnityEngine;

public class BonusScoreCounter : ScoreCounter {


	public GameObject pickedUpEffect;

	void OnTriggerEnter(Collider col){
		base.CountScores ();
		Destroy (gameObject);
		Instantiate (pickedUpEffect, transform.position, Quaternion.identity);
	}
}

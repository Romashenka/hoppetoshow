﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public delegate void PlatformCheck();

[RequireComponent (typeof (Rigidbody))]
public class SphereMotor : MonoBehaviour {
	/*
	Rigidbody myRB;
	Vector3 jumpVelocity;

	public event PlatformCheck OnPlatformChecked;

	public GameObject comboEffect; //эффект при успешном отскоке от 3-й платформы

	public float lowestY; // низшая Y координата. При позиции шара ниже игра заканчивается.

	public float inputSmoothness = 5; //скорость следования шара за пальцем по оси Х

	public float inputSpeed = .5f;

	Vector3 startPosition; // OnTouchStart
	Vector2 touchStartPosition = Vector3.zero;
	Vector3 targetPosition;

	void Start () {
		myRB = GetComponent<Rigidbody> ();
		jumpVelocity = GameManager.instance.gameData.jumpVelocity;
		GameManager.instance.OnGameStarted += Launch;
		touchStartPosition = Vector2.zero;
		startPosition = Vector3.zero;
	}

	void Update () {

		if (Input.touchCount > 0) {
			//float fingerPositionX = GetWorldCoordinatesX (Input.touches [0]);
			//Vector3 targetPosition = Vector3.zero;
			//targetPosition.x = TouchDeltaPosWorld(Input.touches [0].deltaPosition);
			//transform.position = Vector3.Lerp(transform.position,transform.position + TouchDeltaPosWorld(Input.touches [0].deltaPosition),Time.deltaTime * inputSmoothness);
			//Debug.Log ("ball " + transform.position);
			//Debug.Log ("finger" + TouchDeltaPosWorld (Input.touches [0].deltaPosition));
			//targetPosition.x = TouchDeltaPosWorld(Input.touches [0].deltaPosition).x;
			//transform.position += targetPosition;
			//transform.position = Vector3.Lerp (transform.position, targetPosition, inputSmoothness * Time.deltaTime);
			//MoveToFingerSlide(ref startPosition,ref touchStartPosition,Input.touches[0]);

			//MoveToFingerSlide(ref targetPosition,ref startPosition,ref touchStartPosition,Input.touches[0]);
			//transform.position = Vector3.Lerp (transform.position, transform.position+(Vector3)Input.touches[0].deltaPosition*inputSpeed, Time.deltaTime * inputSmoothness);

			Vector3 direction = Input.touches [0].deltaPosition.normalized;
			transform.Translate (direction.x*Time.deltaTime, 0, 0);
		
		
		}

		if (Input.GetAxis ("Horizontal")!=0) {
			Vector3 direction = transform.position;
			direction.x += Input.GetAxisRaw ("Horizontal");
			transform.position = Vector3.Lerp (transform.position, direction, inputSmoothness * Time.deltaTime);
		}

		if (transform.position.y < lowestY) {
			GameManager.instance.EndGame ();
		}
	}



	void MoveToFingerSlide(ref Vector3 targetPosition ,ref Vector3 startPosition, ref Vector2 touchStartPosition, Touch touch){
		
		if (touch.phase == TouchPhase.Began) {
			startPosition = Camera.main.WorldToScreenPoint(transform.position);
			touchStartPosition = Input.touches [0].position;
		}
		if(touch.phase == TouchPhase.Moved){
			targetPosition = startPosition + (Vector3)(touch.position - touchStartPosition)*inputSpeed;
			targetPosition = Camera.main.ScreenToWorldPoint (targetPosition);
		}
		targetPosition.y = transform.position.y;
		targetPosition.z = transform.position.z;

	}


	Vector3 TouchDeltaPosWorld(Vector2 touchDelta){
		Vector3 deltaPosition = touchDelta;
		deltaPosition.z = Camera.main.WorldToScreenPoint (transform.position).z;
		Vector3 result = Vector3.zero;
		result.x = Camera.main.ScreenToWorldPoint (deltaPosition).x;
		return result;
	}

	// координаты касания по оси Х на уровне шара
	float GetWorldCoordinatesX(Touch touch){
		Vector3 touchPosition = touch.position;
		Vector3 transformPositionInPix = Camera.main.WorldToScreenPoint (transform.position);
		touchPosition.z = transformPositionInPix.z;
		float result = Camera.main.ScreenToWorldPoint (touchPosition).x;
		return result;
	}

	void OnCollisionEnter(Collision collision){
		if (collision.gameObject.tag == "Platform") {
			StartCoroutine (PushOnPlatform ());
			GameManager.instance.gameData.scoreCount++;
			if (GameManager.instance.gameData.scoreCount % 3 == 0)
				Instantiate (comboEffect, transform.position, comboEffect.transform.rotation, transform);
			if (OnPlatformChecked != null) {
				OnPlatformChecked ();
			}
		}
	}


	IEnumerator PushOnPlatform(){
		yield return new WaitForSeconds (GameManager.instance.gameData.platformPushDownTime);

		Launch ();
	}

	//Триггеры - монеты
	void OnTriggerEnter(Collider collider){
		GameManager.instance.gameData.coinsCount++;
		Destroy (collider.gameObject);
	}

	void Launch(){
		myRB.velocity = jumpVelocity;
	}
	*/
}



